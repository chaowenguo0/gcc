FROM buildpack-deps
ARG thread
RUN ["apt", "update"]
RUN ["apt", "install", "-y", "--no-install-recommends", "flex", "autogen"]
WORKDIR /
RUN ["git", "clone", "https://gcc.gnu.org/git/gcc.git"]
WORKDIR /gcc
RUN ["./contrib/download_prerequisites"]
#RUN ["rm", "-rf", "/gcc/mpfr/tests/tversion.c"]
WORKDIR /gcc/build
RUN ["../configure", "--enable-threads=posix", "--enable-checking=release", "--disable-multilib", "--enable-languages=c,c++"] #--enable-__cxa_atexit
RUN ["bash", "-c", "make -j $thread"]
#ENV LD_LIBRARY_PATH /usr/local/lib
RUN ["make", "check"]
RUN ["make", "install-strip"]
WORKDIR /
RUN ["rm", "-rf", "gcc"]
