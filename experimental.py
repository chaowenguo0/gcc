import pathlib, json, asyncio

async def f():
    daemon = pathlib.Path('/etc/docker/daemon.json')
    daemon.write_text(json.dumps(json.loads(daemon.read_text()) | {'experimental':True}))
    process = await asyncio.create_subprocess_exec('systemctl', 'restart', 'docker')
    await process.wait()

asyncio.run(f())
#docker version
